# Flutter Widget from HTML

A Flutter plugin for building Flutter-widget tree from html.

This package extends the [`flutter_widget_from_html_core`](https://pub.dev/packages/flutter_widget_from_html_core) package with extra functionalities by using external depedencies like `cached_network_image` or `url_launcher` but add action click image URL and check color from `rgb` or `hexadecimal`. It should be good enough as a quick starting point but you can always use the `core` directly if you dislike the dependencies. Tks `daohoangson` for share!

## Example
```dart
const _html = """
<p><em style=\"color: rgb(255,0,0);\">Dự án mạng xã hội Lotus được thành lập, đầu tư và triển khai bởi VCCorp cùng sự tham gia vốn của các doanh nghiệp và cá nhân trong nước. </em><strong style=\"color: rgb(230, 0, 0);\"><em>Ngay từ cái nhìn đầu tiên, Lotus đã được định hướng mang trong mình điểm khác biệt đặc trưng khi lấy nội dung chất lượng làm trọng tâm, tạo nên một nền tảng hỗ trợ các nhà sản xuất nội dung (content creator) và bất kỳ cá nhân người dùng nào khác thỏa sức sáng tạo.</em></strong></p>
<p><em>Được thiết kế bởi đội ngũ hơn 200 kỹ sư công nghệ của VCCorp cùng nhóm chuyên gia trải nghiệm người dùng, Lotus là tinh hoa kết tụ từ nhiều lĩnh vực tiên tiến như ứng dụng di động, trí tuệ nhân tạo, Big Data, điện toán đám mây... Tổng huy động cho Lotus tính tới nay đã lên tới 700 tỷ đồng từ VCCorp và các nhà đầu tư trong nước, dự kiến kêu gọi thêm 500 tỷ tiếp theo cho giai đoạn khởi chạy đầu tiên, tạo nền móng vững chắc cho tương lai phát triển lâu dài.</em></p>
<p><strong><em>Website truy cập mạng xã hội Lotus:&nbsp;</em></strong><a href=\"https://lotus.vn/\" target=\"_blank\"><em>https://lotus.vn/</em></a></p>
<p><br></p><p><a href=\"https://www.youtube.com/watch?v=1o2t7ga3yPs\" target=\"_blank\">https://www.youtube.com/watch?v=1o2t7ga3yPs</a></p><p><br></p><p><br></p>
<p>And YouTube video!</p>
<iframe src="https://www.youtube.com/embed/jNQXAC9IVRw" width="560" height="315"></iframe>
""";

class HelloWorldScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text('HelloWorldScreen'),
        ),
        body: HtmlWidget(_html, webView: true, onTapUrl: (url) {
                          if (checkImageExtension(url))
                            openGallery(context, [url]);
                          else
                            launchAction(url);
                        }),
      );
}
```

## Extensibility

See [flutter_widget_from_html_core](https://pub.dev/packages/flutter_widget_from_html_core#extensibility) for details.
