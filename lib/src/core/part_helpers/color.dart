part of '../core_helpers.dart';

const kCssColor = 'color';

final _colorRegExp = RegExp(r'^#([a-f0-9]{3,8})$', caseSensitive: false);

Color parseColor(String value) {
  if (value == 'transparent') return null;

  final match = _colorRegExp.firstMatch(value);

  if (match == null) {
    value = value.replaceAll('rgb(', '').replaceAll(')', '');
    var _int = value.split(',');
    return Color.fromRGBO(
        int.parse(_int[0]), int.parse(_int[1]), int.parse(_int[2]), 1);
  } else {
    final hex = match[1].toUpperCase();
    switch (hex.length) {
      case 3:
        return Color(int.parse("0xFF${_x2(hex)}"));
      case 4:
        final alpha = hex[3];
        final rgb = hex.substring(0, 3);
        return Color(int.parse("0x${_x2(alpha)}${_x2(rgb)}"));
      case 6:
        return Color(int.parse("0xFF$hex"));
      case 8:
        final alpha = hex.substring(6, 8);
        final rgb = hex.substring(0, 6);
        return Color(int.parse("0x$alpha$rgb"));
    }
  }

  return null;
}

String convertColorToHex(Color value) {
  final r = value.red.toRadixString(16).padLeft(2, '0');
  final g = value.green.toRadixString(16).padLeft(2, '0');
  final b = value.blue.toRadixString(16).padLeft(2, '0');
  final a = value.alpha.toRadixString(16).padLeft(2, '0');
  return "#$r$g$b$a";
}

String _x2(String value) {
  final sb = StringBuffer();
  for (var i = 0; i < value.length; i++) {
    sb.write(value[i] * 2);
  }
  return sb.toString();
}
