import 'package:flutter/widgets.dart';

import 'core_html_widget.dart';
import 'core_widget_factory.dart';
import 'data_classes.dart';

part 'part_helpers/border.dart';

part 'part_helpers/color.dart';

part 'part_helpers/css.dart';

const placeholderWidget = const SizedBox.shrink();

typedef void OnTapUrl(String url);

typedef WidgetFactory FactoryBuilder(BuildContext context, HtmlWidget widget);
